#!/bin/sh
set -euo pipefail


#
## Variables
#
NAMED_DIR="/etc/bind"
NAMED_CONF="${NAMED_DIR}/named.conf"

DEBUG_ENTRYPOINT="${DEBUG_ENTRYPOINT:-0}"

USER="named"
GROUP="named"
PUID="${PUID:-250}"
PGID="${PGID:-250}"

if [[ -d /var/log/named ]]; then
	NAMED_LOGDIR="/var/log/named"
else
	NAMED_LOGDIR=""
fi

#
## Log to stdout/stderr
#
log () {
	local type="${1}"	# ok, warn or err
	local message="${2}"	# msg to print
	local debug="${3}"	# 0: only warn and error, >0: ok and info

	local clr_ok="\033[0;32m"
	local clr_info="\033[0;36m"
	local clr_warn="\033[0;033m"
	local clr_err="\033[0;31m"
	local clr_rst="\033[0m"
	
	if [[ "${type}" = ok ]]; then
		if [[ "${debug}" -gt 0 ]]; then
			printf "${clr_ok}[OK]   %s${clr_rst}\n" "${message}"
		fi
	elif [[ "${type}" = info ]]; then
		if [[ "${debug}" -gt 0 ]]; then
			printf "${clr_info}[INFO] %s${clr_rst}\n" "${message}"
		fi
	elif [[ "${type}" = warn ]]; then
		printf "${clr_warn}[WARN] %s${clr_rst}\n" "${message}" 1>&2	# stdout -> stderr
	elif [[ "${type}" = err ]]; then
		printf "${clr_err}[ERR]  %s${clr_rst}\n" "${message}" 1>&2	# stdout -> stderr
	else
		printf "${clr_err}[???]  %s${clr_rst}\n" "${message}" 1>&2	# stdout -> stderr
	fi
}


#
## Set debug level
#
if ! echo "${DEBUG_ENTRYPOINT}" | grep -Eq '^[-+]?[0-9]+$'; then
        log "warn" "Wrong value for DEBUG_ENTRYPOINT: '${DEBUG_ENTRYPOINT}'. Setting to value '1'." "2"
        DEBUG_ENTRYPOINT=1
elif [[ "${DEBUG_ENTRYPOINT}" -lt 0 ]]; then
        log "warn" "Wrong value for DEBUG_ENTRYPOINT: '${DEBUG_ENTRYPOINT}'. Setting to value '1'." "2"
        DEBUG_ENTRYPOINT=1
elif [[ "${DEBUG_ENTRYPOINT}" -gt 1 ]]; then
        log "warn" "Wrong value for DEBUG_ENTRYPOINT: '${DEBUG_ENTRYPOINT}'. Setting to value '1'." "2"
        DEBUG_ENTRYPOINT=1
fi
log "info" "Debug level: ${DEBUG_ENTRYPOINT}" "${DEBUG_ENTRYPOINT}"


#
## Check and change named UID and GID
#
if [[ "$( id -g "${GROUP}" )" -ne "${PGID}" || "$( id -u "${USER}" )" -ne "${PUID}" ]]; then
	log "info" "Set UID and GID." "${DEBUG_ENTRYPOINT}"
	uid_old="$( id -u "${USER}" )"
	gid_old="$( id -u "${GROUP}" )"
	deluser "${USER}"
	addgroup -g "${PGID}" "${GROUP}"
	adduser -u "${PUID}" -G "${GROUP}" -h "${NAMED_DIR}" -g 'ISC Bind user named' -s /sbin/nologin -D "${USER}"

	log "info" "Set ownership and permissions for old UID/GID files." "${DEBUG_ENTRYPOINT}"
	find / -not \( -path /proc -prune \) -not \( -path /sys -prune \) -user "${uid_old}" -exec chown "${USER}" {} \;
	find / -not \( -path /proc -prune \) -not \( -path /sys -prune \) -group "${gid_old}" -exec chgrp "${GROUP}" {} \;
fi


#
## Set ownership and permissions
#
log "info" "Set ownership and permissions on bind direcotries." "${DEBUG_ENTRYPOINT}"
chown -R root:"${GROUP}" "${NAMED_DIR}" /var/bind /var/run/named ${NAMED_LOGDIR} 
chmod -R g+w /var/bind /var/run/named ${NAMED_LOGDIR} 
chmod -R o-rwx "${NAMED_DIR}" /var/bind /var/run/named ${NAMED_LOGDIR}


#
## Check named.conf and start bind
#
if ! output="$( named-checkconf "${NAMED_CONF}" 2>&1 )"; then
	log "err" "Configuration failed." "${DEBUG_ENTRYPOINT}"
	printf "${output}\n"
	exit
fi


#
## Start ISC Bind
#
log "info" "Starting $( named -V | grep -oiE '^BIND[[:space:]]+[0-9.]+' )" "${DEBUG_ENTRYPOINT}"
exec /usr/sbin/named -4 -c "${NAMED_CONF}" -u "${USER}" -f
