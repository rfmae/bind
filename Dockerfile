FROM alpine:3.12

RUN apk add --no-cache bind 

COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod 750 /docker-entrypoint.sh

EXPOSE 53/udp 53/tcp

ENTRYPOINT ["/docker-entrypoint.sh"]
