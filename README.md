# ISC Bind 9 Docker

This docker image provides a DNS server based on [ISC Bind 9](https://www.isc.org/bind/) and [Alpine Linux](https://www.alpinelinux.org).
 
## Docker Environment Variables

You are able to pass variables via docker command to the container.

| Variable           | Type   | Default   | Description |
|--------------------|--------|-----------|-------------|
| `DEBUG_ENTRYPOINT` | bool   | `0`       | Show shell commands executed during start.<br/>Values: `0` or `1` |
| `PUID`             | int    | `250`     | Specify custom UID for the user `named`. |
| `PGID`             | int    | `250`     | Specify custom GID for the group `named`. |

## Usage

To run this image you have to provide your own configuration and zone files. An example [configuration](example/named.conf "named.conf") and [zone](example/example.com.zone "example.com.zone") file is provided in the [example](example) directory.

### Docker run
```
docker run -d --name bind \
  -p 53:53/udp -p 53:53 \
  -v <bind-conf>:/etc/bind \
  -v <bind-zones>:/var/bind \
  -v <bind-log>:/var/log/named \
  --restart=always registry.gitlab.com/rfmae/bind
```

### Bind configuration

To configure ISC Bind mount the directory containing your configuration files into the docker container using the option:
```
-v <bind-conf>:/etc/bind
```

Use the same option to mount the direcotry containing your zone files into the docker container:
```
-v <bind-zones>:/var/bind
```

Write the Bind logfiles to a persistent directory on the host:
```
-v <bind-logs>:/var/log/named
```
